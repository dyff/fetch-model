# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

stages:
  - test
  - build
  - deploy

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_OPEN_MERGE_REQUESTS
      when: never
    - when: always

include:
  - project: buildgarden/pipelines/gitlab
    ref: 0.2.2
    file:
      - gitlab-release.yml
  - project: buildgarden/pipelines/detect-secrets
    ref: 0.2.1
    file:
      - detect-secrets.yml
  - project: buildgarden/pipelines/container
    ref: 0.3.1
    file:
      - container-docker.yml
      - container-hadolint.yml
  - project: buildgarden/pipelines/prettier
    ref: 0.3.2
    file:
      - prettier.yml
  - project: buildgarden/pipelines/skywalking-eyes
    ref: 0.2.2
    file:
      - license-eye-header-check.yml
  - project: buildgarden/pipelines/python
    ref: 0.14.0
    file:
      - python-isort.yml # import sorter
      - python-black.yml # code formatter:
      - python-vulture.yml # find dead code
      - python-pytest.yml

python-pytest:
  variables:
    PYTHON_REQUIREMENTS: -r requirements.txt
    PYTHON_PACKAGE: fetch_model
  before_script:
    - |-
      if [[ -n "$PYTHON_PYPI_GITLAB_GROUP_ID" ]] ; then
        export PYTHON_PYPI_DOWNLOAD_URL="https://${PYTHON_PYPI_USERNAME}:${PYTHON_PYPI_PASSWORD}@${CI_SERVER_HOST}/api/v4/groups/${PYTHON_PYPI_GITLAB_GROUP_ID}/-/packages/pypi/simple"
        echo "Pulling PyPI packages from GitLab group ID $PYTHON_PYPI_GITLAB_GROUP_ID"
      elif [[ -n "$PYTHON_PYPI_GITLAB_PROJECT_ID" ]] ; then
        export PYTHON_PYPI_DOWNLOAD_URL="https://${PYTHON_PYPI_USERNAME}:${PYTHON_PYPI_PASSWORD}@${CI_SERVER_HOST}/api/v4/projects/${PYTHON_PYPI_GITLAB_PROJECT_ID}/packages/pypi/simple"
        echo "Pulling PyPI packages from GitLab project ID $PYTHON_PYPI_GITLAB_PROJECT_ID"
      fi
    - |-
      if [[ -n "$PYTHON_PYPI_DOWNLOAD_URL" ]] ; then
      cat > "$PIP_CONFIG_FILE" <<EOF
      [global]
      index-url = ${PYTHON_PYPI_DOWNLOAD_URL}
      EOF
      fi
    - apt-get update
    - apt-get install -y git git-lfs
