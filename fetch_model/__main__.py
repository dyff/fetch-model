# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

import argparse

# mypy: disable-error-code="import-untyped"
import contextlib
import datetime
import logging
import tempfile

import ruamel.yaml

from .fetch import fetch
from .models import ModelSource


def _local_dir_context(local_dir: str):
    if local_dir is not None:
        return contextlib.nullcontext(local_dir)
    else:
        return tempfile.TemporaryDirectory()


def main(model_yaml: str, local_dir: str, debug_log_disk_usage: bool = False):
    if debug_log_disk_usage:
        import subprocess
        import threading
        import time

        def _watch_du():
            while True:
                subprocess.run(
                    "for d in /*; do du -sh $d; done", shell=True, check=True
                )
                time.sleep(1.0)

        monitor_daemon = threading.Thread(
            target=_watch_du, daemon=True, name="watch_du"
        )
        monitor_daemon.start()

    yaml = ruamel.yaml.YAML()
    with open(model_yaml, "r") as fin:
        model = yaml.load(fin)

    with _local_dir_context(local_dir) as local_dir:
        source = ModelSource.parse_obj(model["spec"]["source"])

        ts_start = datetime.datetime.now(datetime.timezone.utc)
        logging.info(f"fetch started: {ts_start}")

        fetch(local_dir, source)

        ts_finish = datetime.datetime.now(datetime.timezone.utc)
        logging.info(f"fetch finished: {ts_finish}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="fetch_model", description="Fetch a model")
    parser.add_argument(
        "--model-yaml",
        required=True,
        help="Path to a YAML file containing the Model manifest.",
    )
    parser.add_argument(
        "--local-dir",
        required=False,
        default=None,
        help="""Directory to fetch the model into. This can be an ephemeral directory if
        you just want the model to end up in s3, or a presistent volume if you
        want the model in a PVC. If not specified, a temp directory will be created.""",
    )
    parser.add_argument(
        "--debug-log-disk-usage",
        action="store_true",
        help="""Run a background thread that prints disk usage information.
        Useful for figuring out where BentoML / OpenLLM are storing things.""",
    )
    args = parser.parse_args()

    main(
        model_yaml=args.model_yaml,
        local_dir=args.local_dir,
        debug_log_disk_usage=args.debug_log_disk_usage,
    )
