# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

import enum
from typing import Any, Optional

import pydantic


class DyffSchemaBaseModel(pydantic.BaseModel):
    """Base class for pydantic models that used for defining data schemas.

    Overrides serialization functions to serialize by alias, so that
    "round-trip" serialization is the default for fields with aliases. We
    prefer aliases because we can 1) use _underscore_names_ as reserved names
    in our data schema, and 2) allow Python reserved words like 'bytes' as
    field names.
    """

    def dict(self, *, by_alias: bool = True, **kwargs) -> dict[str, Any]:
        return super().dict(by_alias=by_alias, **kwargs)

    def json(self, *, by_alias: bool = True, **kwargs) -> str:
        return super().json(by_alias=by_alias, **kwargs)


class ModelSourceKinds(str, enum.Enum):
    GitLFS = "GitLFS"
    HuggingFaceHub = "HuggingFaceHub"
    OpenLLM = "OpenLLM"
    Upload = "Upload"


class ModelSourceGitLFS(DyffSchemaBaseModel):
    url: pydantic.HttpUrl = pydantic.Field(
        description="The URL of the Git LFS repository"
    )


class ModelSourceHuggingFaceHub(DyffSchemaBaseModel):
    """These arguments are forwarded to huggingface_hub.snapshot_download()"""

    repoID: str
    revision: str
    allowPatterns: Optional[list[str]] = None
    ignorePatterns: Optional[list[str]] = None


class ModelSourceOpenLLM(DyffSchemaBaseModel):
    modelKind: str = pydantic.Field(
        description="The kind of model (c.f. 'openllm build <modelKind>')"
    )

    modelID: str = pydantic.Field(
        description="The specific model identifier (c.f. 'openllm build ... --model-id <modelId>')",
    )

    modelVersion: str = pydantic.Field(
        description="The version of the model (e.g., a git commit hash)"
    )


class ModelSource(DyffSchemaBaseModel):
    kind: ModelSourceKinds = pydantic.Field(description="The kind of model source")

    gitLFS: Optional[ModelSourceGitLFS] = pydantic.Field(
        default=None, description="Specification of a Git LFS source"
    )

    huggingFaceHub: Optional[ModelSourceHuggingFaceHub] = pydantic.Field(
        default=None, description="Specification of a HuggingFace Hub source"
    )

    openLLM: Optional[ModelSourceOpenLLM] = pydantic.Field(
        default=None, description="Specification of an OpenLLM source"
    )
