# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from .models import ModelSource, ModelSourceKinds


def fetch(local_path: str, source: ModelSource):
    if source.kind == ModelSourceKinds.GitLFS:
        if source.gitLFS is None:
            raise ValueError("gitLFS not specified")
        from .sources import git_lfs

        return git_lfs.fetch(local_path, source.gitLFS)
    elif source.kind == ModelSourceKinds.HuggingFaceHub:
        if source.huggingFaceHub is None:
            raise ValueError("huggingFaceHub not specified")
        from .sources import huggingface_hub

        return huggingface_hub.fetch(local_path, source.huggingFaceHub)
    elif source.kind == ModelSourceKinds.OpenLLM:
        if source.openLLM is None:
            raise ValueError("openLLM not specified")
        from .sources import open_llm

        return open_llm.fetch(local_path, source.openLLM)
    elif source.kind == ModelSourceKinds.Upload:
        pass
    else:
        raise ValueError(str(source))
