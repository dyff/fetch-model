# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

"""
Test fetch model of kind HuggingFaceHub.
"""

import fetch_model
import fetch_model.fetch
import fetch_model.models

raw_source = """ {"kind": "HuggingFaceHub",
        "huggingFaceHub":{
            "repoID":"distilbert/distilbert-base-uncased-finetuned-sst-2-english",
            "revision":"c1f7ee188a6a21a2a8d090e2dcb553f015aca548"}
        }"""


# upload does not do anything, check no local directory set
def test_hugging_face_hub(tmp_path):
    source = fetch_model.models.ModelSource.model_validate_json(raw_source)
    fetch_model.fetch.fetch(str(tmp_path), source)

    model_list = list(tmp_path.iterdir())
    assert len(model_list) == 3
