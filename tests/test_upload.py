# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

"""
Test fetch model of kind upload.
"""

import subprocess


# upload does not do anything, check no local directory set
def test_upload():
    result = subprocess.run(
        ["python", "-m", "fetch_model", "--model-yaml", "tests/fixtures/upload.yml"],
        capture_output=True,
    )
    assert result.returncode == 0
