# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

"""
Test fetch model of kind GitLFS.
"""

import subprocess


# GitLFS clones with git-lfs, also check --local-dir through main, --debug-log-disk-usage
# TODO: use repo using git-lfs for model, update
def test_git_lfs(tmp_path):
    result = subprocess.run(
        [
            "python",
            "-m",
            "fetch_model",
            "--model-yaml",
            "tests/fixtures/git-lfs.yml",
            "--local-dir",
            tmp_path,
            "--debug-log-disk-usage",
        ],
        capture_output=True,
    )
    assert result.returncode == 0
    assert len(list(tmp_path.iterdir())) == 14
